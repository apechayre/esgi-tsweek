import {Pokemon, Stat} from "../Model/Pokemon";
import {Move} from "../Model/Move";
import * as moveRepository from "../Repository/MoveRepository";

export function getStatByName(stats: Stat[], statName: string) :Stat {
    let found = stats.find((stat) => {
        if (stat.stat.name === statName) {
            return true
        }
    });

    if (!found) {
        throw new Error(`Unknown statname : ${statName}`);
    }

    return found;
}

export function pickRandomMove(moves: Move[]) :Promise<Move> {
    let move = moves[Math.ceil(Math.random() * moves.length) -1];

    if (move.id === undefined) {
        return moveRepository.hydrate(move);
    }
    return Promise.resolve(move);
}
