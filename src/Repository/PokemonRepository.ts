import {Pokemon} from "../Model/Pokemon";
import {getPokemonByID} from "../ApiConnector"

export async function getNewPokemon(id?: number) :Promise<Pokemon> {
    id = id ? id : Math.ceil(Math.random() * 50);

    try {
        const response = await getPokemonByID(id);

        return <Pokemon>response.data;
    } catch (error) {
        console.error(error);

        return error;
    }
}
