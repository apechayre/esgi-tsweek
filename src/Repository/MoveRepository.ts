import {Move} from "../Model/Move";
import {getURL} from "../ApiConnector"

export async function hydrate(move: Move) :Promise<Move> {
    try {
        const response = await getURL(move.move.url);

        return <Move>response.data;
    } catch (error) {
        console.error(error);

        return error;
    }
}
