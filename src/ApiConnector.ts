import axios from "axios";

export async function getPokemonByID(id: number) :Promise<any> {
    return axios.get(`https://pokeapi.co/api/v2/pokemon/${id}/`);
}

export async function getURL(url: string) :Promise<any> {
    return axios.get(url);
}
