import {LinkedID} from "./Common"
import {Move} from "./Move"

export interface Ability {
    is_hidden: boolean;
    slot: number;
    ability: LinkedID;
}

export interface GameIndice {
    game_index: number;
    version: LinkedID;
}

export interface HeldItemVersionDetail {
    rarity: number;
    version: LinkedID;
}

export interface HeldItem {
    item: LinkedID;
    version_details: HeldItemVersionDetail[];
}

export interface Sprites {
    back_female: string;
    back_shiny_female: string;
    back_default: string;
    front_female: string;
    front_shiny_female: string;
    back_shiny: string;
    front_default: string;
    front_shiny: string;
}

export interface Stat {
    base_stat: number;
    effort: number;
    stat: LinkedID;
}

export interface Type {
    slot: number;
    type: LinkedID;
}

export interface Pokemon {
    id: number;
    hp: number;
    name: string;
    base_experience: number;
    height: number;
    is_default: boolean;
    order: number;
    weight: number;
    abilities: Ability[];
    forms: LinkedID[];
    game_indices: GameIndice[];
    held_items: HeldItem[];
    moves: Move[];
    species: LinkedID;
    sprites: Sprites;
    stats: Stat[];
    types: Type[];
}
