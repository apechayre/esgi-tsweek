import {LinkedID} from "./Common"

interface EffectEntry {
    effect: string;
    short_effect: string;
    language: LinkedID;
}

interface Meta {
    ailment: LinkedID;
    category: LinkedID;
    min_hits?: any;
    max_hits?: any;
    min_turns?: any;
    max_turns?: any;
    drain: number;
    healing: number;
    crit_rate: number;
    ailment_chance: number;
    flinch_chance: number;
    stat_chance: number;
}

interface Name {
    name: string;
    language: LinkedID;
}

interface SuperContestEffect {
    url: string;
}

interface FlavorTextEntry {
    flavor_text: string;
    language: LinkedID;
    version_group: LinkedID;
}

export interface MoveVersionGroupDetail {
    level_learned_at: number;
    version_group: LinkedID;
    move_learn_method: LinkedID;
}

export interface Move {
    id: number;
    move: LinkedID;
    version_group_details: MoveVersionGroupDetail[];
    name: string;
    accuracy: number;
    effect_chance?: any;
    pp: number;
    priority: number;
    power: number;
    damage_class: LinkedID;
    effect_entries: EffectEntry[];
    effect_changes: any[];
    generation: LinkedID;
    meta: Meta;
    names: Name[];
    past_values: any[];
    stat_changes: any[];
    super_contest_effect: SuperContestEffect;
    target: LinkedID;
    type: LinkedID;
    flavor_text_entries: FlavorTextEntry[];
}
