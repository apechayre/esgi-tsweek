export interface LinkedID {
    name: string;
    url: string;
}
