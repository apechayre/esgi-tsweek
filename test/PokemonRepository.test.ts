/// <reference types="jest" />

import {getNewPokemon} from "../src/Repository/PokemonRepository";

test("get pokemon from pokapi", () =>{
    return getNewPokemon(4).then(poke => {
        expect(poke.id).toBe(4);
        expect(poke.name).toBe("charmander");
    }) ;
});

test("get no from pokapi", () =>{
    return getNewPokemon(-4).then(poke => {
        expect(poke.name).toBe("Error");
    }) ;
});
